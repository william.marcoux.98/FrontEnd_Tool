<?php


?>


<!DOCTYPE html>

<html>

<head>

<title> Web dev tools </title>
<link rel="stylesheet" type="text/css" href="stylesheet.css">


</head>

<body>
 <div id="header">
 <h1>Web Development Tools</h1>
 </div>

 <div id="bezierGenerator">
 <h1> Cubic Bezier Generator </h1>
  <div class="leftFloat">
 
 <canvas id="mainCanvas" width="400" height="400"></canvas><br>
 <br>
  <p class="leftFloat" style="margin-top: 0px; margin-left: 24px;" title="Progression in real time"> &#x1f550 </p>
	<div class="progressBarContainer"title="Progression in real time">
	<div class="progressSquare">
	</div>
	</div>

	<br>
	 <p class="leftFloat" style="margin-top: 3px; margin-left: 30px;" title="Progression according to modelised cubic-bezier"> &#9654 </p>
	<div class="progressBarContainer" title="Progression according to modelised cubic-bezier">
	<div class="animModelSquare">
	</div>
	</div>
	<br>
 
 <div class="myButtons">
 
 <table>
	<th><button class="toggleButton" title="Start"> &#9658 </button></th>
	<th><button class="stopButton" title="Stop" onclick="stopAnim()"> &#9724 </button></th>
 </table>
 </div>
 </div>
 <br>
 <div>
 <table id="paramTab">
 <tr>
	<th style="text-decoration: underline; font-size: 0.8em;">Parameters:</th>
 </tr>
 <tr>
	<th title ="X axis of first point">X<sub>1</sub></th>
	<th title ="Y axis of first point">Y<sub>1</sub></th>
	<th title ="X axis of second point">X<sub>2</sub></th>
	<th title ="Y axis of second point">Y<sub>2</sub></th>
	<th title="Duration (in seconds)">&#x1f550</th>
 </tr>
 <tr>
	<th><input id="num1" type="number" max="100" min="0" onchange="generateBezierCurve(1)"></th>
	<th><input id="num2" type="number" max="100" min="0" onchange="generateBezierCurve(1)"></th>
	<th><input id="num3" type="number" max="100" min="0" onchange="generateBezierCurve(1)"></th>
	<th><input id="num4" type="number" max="100" min="0" onchange="generateBezierCurve(1)"></th>
	<th><input id="num5" type="number" min="0" style="width:70px"></th>
 </tr>
 <tr>
	<th style="text-decoration: underline; font-size: 0.8em;">Common:</th>
 </tr>
 <tr>
	<th> <img onclick="iconClick(0)" class="bezierIcons" src="bezierIcons/linear1.png" height="60" width="60" title="Generate linear curve"> </th>
	<th> <img onclick="iconClick(1)" class="bezierIcons" src="bezierIcons/ease1.png" height="60" width="60" title="Generate ease curve"> </th>
	<th> <img onclick="iconClick(2)" class="bezierIcons" src="bezierIcons/ease-in1.png" height="60" width="60" title="Generate ease-in curve"> </th>
	<th> <img onclick="iconClick(3)" class="bezierIcons" src="bezierIcons/ease-out1.png" height="60" width="60" title="Generate ease-out curve"> </th>
	<th> <img onclick="iconClick(4)" class="bezierIcons" src="bezierIcons/ease-in-out1.png" height="60" width="60" title="Generate ease-in-out curve"> </th>
 </tr>
</table>
</div>

<br>

<div>
<div class="result">
 <p style=" text-decoration: underline; font-size: 0.8em; "> Generated command: </p>
 <p id='finalResult'>cubic-bezier()</p>
 <p onclick="clipBoardBezier('finalResult')" title="Copy to clipboard" class="clipBoard">&#128203</p> 
</div>
</div>
 </div>

<div id="colorPicker">
	<h1> Color picker </h1>
<input type="color" name="colorPicked" class="colorMainInput" onchange="colorUpdate(1)">

<div id="updateList">
<ul id="colorRanges">
<li><h2 style="display:inline;">R</h2><input type="range" name="redLvl" id="redLvl" onchange="colorUpdate(2)" min="0" max="255"> <input type="number" id="redNum" min="0" max="255" onchange="colorUpdate(3)"></li>
<li><h2 style="display:inline;">G</h2><input type="range" name="greenLvl" id="greenLvl" onchange="colorUpdate(2)" min="0" max="255"> <input type="number" id="greenNum" min="0" max="255" onchange="colorUpdate(3)"> </li>
<li><h2 style="display:inline;">B</h2><input type="range" name="blueLvl" id="blueLvl" onchange="colorUpdate(2)" min="0" max="255"> <input type="number" id="blueNum" min="0" max="255" onchange="colorUpdate(3)"></li>
<li><h2 style="display:inline;">#</h2><input type="text" id="hexGen" value="ffffff"><button onclick="colorUpdate(4)">Use code</button></li>
</ul>
<p style=" margin-left: 45px; text-decoration: underline; font-size: 0.8em; font-weight: bolder; "> Generated command: </p>
 <p id='finalColor'>rgb();</p>
 <p onclick="clipBoardBezier('finalColor')" style="margin-left: 45px;" title="Copy to clipboard" class="clipBoard">&#128203</p> 
</div>

</div>

<div id="fontViewer">
	<h1>Font viewer</h1>
	
	<table> <tr>
	
	<td><div id="textTemplate">
		<p id="testText"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>	
	</div></td>
	
	<td><div id="textParams">
		<table>
		<tr> <td><h2>font-family:</h2></td> <td><input type="text" class="textMod"></td> <td><button title="Update text" onclick="updateFont(0)">&#8635</button> </td> </tr>
		<tr> <td><h2>font-weight:</h2></td> <td><input type="text" class="textMod"></td> <td><button title="Update text" onclick="updateFont(1)">&#8635</button> </td> </tr>
		<tr> <td><h2>font-size:</h2></td> <td><input type="text" class="textMod"></td> <td><button title="Update text" onclick="updateFont(2)">&#8635</button> </td> </tr>
		<tr> <td><h2>color:</h2></td> <td><input type="text" class="textMod"></td> <td><button title="Update text" onclick="updateFont(3)">&#8635</button> </td> </tr>
		</table>
	</div></td>
	</tr></table>


</div>
 
<script type="text/javascript" src="myscript.js"></script>
</body>

</html>
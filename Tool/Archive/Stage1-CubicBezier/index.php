<!DOCTYPE html>

<html>

<head>

<title> Web dev tools </title>
<link rel="stylesheet" type="text/css" href="stylesheet.css">


</head>

<body>


 <div id="bezierGenerator">
 <h1> Cubic Bezier Generator </h1>
  <div class="leftFloat">
 
 <canvas id="mainCanvas" width="400" height="400"></canvas><br>
 <br>
  <p class="leftFloat" style="margin-top: 0px; margin-left: 24px;" title="Progression in real time"> &#x1f550 </p>
	<div class="progressBarContainer"title="Progression in real time">
	<div class="progressSquare">
	</div>
	</div>

	<br>
	 <p class="leftFloat" style="margin-top: 3px; margin-left: 30px;" title="Progression according to modelised cubic-bezier"> &#9654 </p>
	<div class="progressBarContainer" title="Progression according to modelised cubic-bezier">
	<div class="animModelSquare">
	</div>
	</div>
	<br>
 
 <div class="myButtons">
 
 <table>
	<th><button class="toggleButton" title="Start"> &#9658 </button></th>
	<th><button class="stopButton" title="Stop" onclick="stopAnim()"> &#9724 </button></th>
 </table>
 </div>
 </div>
 <br>
 <table id="paramTab">
 <tr>
	<th style="text-decoration: underline; font-size: 0.8em;">Parameters:</th>
 </tr>
 <tr>
	<th title ="X axis of first point">X<sub>1</sub></th>
	<th title ="Y axis of first point">Y<sub>1</sub></th>
	<th title ="X axis of second point">X<sub>2</sub></th>
	<th title ="Y axis of second point">Y<sub>2</sub></th>
	<th title="Duration (in seconds)">&#x1f550</th>
 </tr>
 <tr>
	<th><input id="num1" type="number" max="100" min="0" onchange="generateBezierCurve(1)"></th>
	<th><input id="num2" type="number" max="100" min="0" onchange="generateBezierCurve(1)"></th>
	<th><input id="num3" type="number" max="100" min="0" onchange="generateBezierCurve(1)"></th>
	<th><input id="num4" type="number" max="100" min="0" onchange="generateBezierCurve(1)"></th>
	<th><input id="num5" type="number" min="0" style="width:70px"></th>
 </tr>
 <tr>
	<th style="text-decoration: underline; font-size: 0.8em;">Common:</th>
 </tr>
 <tr>
	<th> <img onclick="iconClick(0)" class="bezierIcons" src="bezierIcons/linear1.png" height="60" width="60" title="Generate linear curve"> </th>
	<th> <img onclick="iconClick(1)" class="bezierIcons" src="bezierIcons/ease1.png" height="60" width="60" title="Generate ease curve"> </th>
	<th> <img onclick="iconClick(2)" class="bezierIcons" src="bezierIcons/ease-in1.png" height="60" width="60" title="Generate ease-in curve"> </th>
	<th> <img onclick="iconClick(3)" class="bezierIcons" src="bezierIcons/ease-out1.png" height="60" width="60" title="Generate ease-out curve"> </th>
	<th> <img onclick="iconClick(4)" class="bezierIcons" src="bezierIcons/ease-in-out1.png" height="60" width="60" title="Generate ease-in-out curve"> </th>
 </tr>
</table>


<br>

<div class="result">
 <p style=" text-decoration: underline; font-size: 0.8em; "> Generated command: </p>
 <p style="font-size: 1.3em; margin-top: 0px;" id='finalResult'>cubic-bezier()</p> 
 </div>
 </div>  
 
<script type="text/javascript" src="myscript.js"></script>
</body>

</html>
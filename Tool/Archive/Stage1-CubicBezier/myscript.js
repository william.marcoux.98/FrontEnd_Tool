var progressBox = document.getElementsByClassName('progressSquare')[0];
var stopButton = document.getElementsByClassName('stopButton')[0];
var myButton = document.getElementsByClassName("toggleButton")[0];
var animModelSquare = document.getElementsByClassName('animModelSquare')[0];
var toggle = false;
var delayMillis = 1;
var myTimer;
var myTimer2;
document.getElementById('num5').value = 4;
setValues(1,4,37,99);
var result = document.getElementById('finalResult');
var toggleImg = 5;
var toggleIcn = 0;

function createClass(name,rules){
    var style = document.createElement('style');
    style.type = 'text/css';
    document.getElementsByTagName('head')[0].appendChild(style);
    if(!(style.sheet||{}).insertRule) 
        (style.styleSheet || style.sheet).addRule(name, rules);
    else
        style.sheet.insertRule(name+"{"+rules+"}",0);
}


function setValues(num1, num2, num3, num4){
	document.getElementById('num1').value = num1;
	document.getElementById('num2').value = num2;
	document.getElementById('num3').value = num3;
	document.getElementById('num4').value = num4;
}

function resetIcn(){
	for(i = 0; i<5; i++){
		var icn = document.getElementsByClassName("bezierIcons")[i];
		var imgSrc = icn.src;
		icn.src = imgSrc.replace("2", "1");
	}
}

function iconClick(clickedIcn) {
	toggleImg = clickedIcn;
	resetIcn();
	var icn = document.getElementsByClassName("bezierIcons")[clickedIcn];
	var imgSrc = icn.src;
	icn.src = imgSrc.replace("1", "2");
	if(clickedIcn == 0)
		setValues(0,0,100,100);
	else if(clickedIcn == 1)
		setValues(25,10,25,10);
	else if(clickedIcn == 2)
		setValues(42,0,100,100);
	else if(clickedIcn == 3)
		setValues(0,0,58,100);
	else if(clickedIcn == 4)
		setValues(42,0,58,100);
	generateBezierCurve(1);
}
generateBezierCurve(1);

function generateBezierCurve(adaptFrom){
	var canvas = document.getElementById('mainCanvas');
    var ctx = canvas.getContext('2d');
	if(adaptFrom == 1)
	{
		if(toggleImg != 5)
			toggleImg = 5;
		else if(toggleImg = 5)
			resetIcn();

		result.innerHTML = "cubic-bezier(" + (document.getElementById('num1').value /100).toString() +"," + (document.getElementById('num2').value /100).toString() + "," + (document.getElementById('num3').value /100).toString()+ ","+(document.getElementById('num4').value /100).toString() + ");";
		var num1 = document.getElementById('num1').value;
		var num2 = document.getElementById('num2').value;
		var num3 = document.getElementById('num3').value;
		var num4 = document.getElementById('num4').value;
	 }
	 
	var axis1X = 50;
    var axis1Y = canvas.height - 50;
	var axis2X = (canvas.width - 100) * (num1 / 100) + 50;
    var axis2Y = (canvas.height - 100) * (1 - (num2 / 100)) + 50;
	var axis3X = (canvas.width - 100) * (num3 / 100) + 50;
    var axis3Y = (canvas.height - 100) * (1-(num4 / 100)) + 50;
	var axis4X = canvas.width - 50;
    var axis4Y = 50;
	 
	var radius = 10;
	 
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	 
	ctx.beginPath();
	ctx.moveTo(axis2X, axis2Y);
	ctx.lineTo(axis1X, axis1Y);
	ctx.lineWidth = 5;
	ctx.strokeStyle = 'lightgrey';
	ctx.stroke();
	ctx.beginPath();
	ctx.moveTo(axis3X, axis3Y);
	ctx.lineTo(axis4X, axis4Y);
	ctx.lineWidth = 5;
	ctx.strokeStyle = 'lightgrey';
	ctx.stroke();
	 
	ctx.beginPath();
	ctx.moveTo(axis1X, axis1Y);
	ctx.bezierCurveTo(axis2X, axis2Y,axis3X,axis3Y,axis4X,axis4Y);
	ctx.lineWidth = 5;
	ctx.strokeStyle = 'black'; 
	ctx.stroke();
	
	ctx.beginPath();
    ctx.arc(axis2X, axis2Y, radius, 0, 2 * Math.PI, false);
    ctx.fillStyle = 'lightblue';
    ctx.fill();
    ctx.lineWidth = 5;
    ctx.strokeStyle = '#003300';
    ctx.stroke();
	  
    ctx.beginPath();
    ctx.arc(axis3X, axis3Y, radius, 0, 2 * Math.PI, false);
    ctx.fillStyle = 'lightblue';
    ctx.fill();
    ctx.lineWidth = 5;
    ctx.strokeStyle = '#003300';
    ctx.stroke();
	  
    ctx.beginPath();
    ctx.arc(axis1X, axis1Y, radius, 0, 2 * Math.PI, false);
    ctx.fillStyle = 'white';
    ctx.fill();
    ctx.lineWidth = 5;
    ctx.strokeStyle = '#003300';
    ctx.stroke();

    ctx.beginPath();
    ctx.arc(axis4X, axis4Y, radius, 0, 2 * Math.PI, false);
    ctx.fillStyle = 'white';
    ctx.fill();
    ctx.lineWidth = 5;
    ctx.strokeStyle = '#003300';
    ctx.stroke();
}

myButton.onclick = function() {
	var duration = document.getElementById('num5').value.toString();
	var properties = "animation-name: progressBar; animation-duration: " + duration + "s;animation-timing-function: linear; margin-left: 0%;";
	createClass('.animToAttach',properties);
	
	var cubicBezierModel = (document.getElementById('num1').value /100).toString() +"," + (document.getElementById('num2').value /100).toString() + "," + (document.getElementById('num3').value /100).toString()+ ","+(document.getElementById('num4').value /100).toString();
	properties = "animation-name: animModel; animation-duration: " + duration + "s;animation-timing-function: cubic-bezier("+cubicBezierModel+ ");";
	createClass('.animDemo', properties);
	
	stopButton.style.color = "red";
  if(toggle===false) 
  {
    this.innerHTML = "<span style='color:blue;'>&#8635</span>";
	this.title = "Restart";
    progressBox.classList.add('animToAttach');
	animModelSquare.classList.add('animDemo');
	toggle = true;
  } 
  else if (toggle===true)
  {
	clearTimeout(myTimer);
	delayMillis = 1;
    progressBox.classList.remove('animToAttach');
	animModelSquare.classList.remove('animDemo');

		myTimer2 = setTimeout(function()
			{
			   animModelSquare.classList.add('animDemo');
			   progressBox.classList.add('animToAttach');	
			}, delayMillis);
  }  
  timer();
}

function stopAnim() {
	clearTimeout(myTimer);
	myButton.innerHTML = "&#9658";
	myButton.title = "Start";
	progressBox.classList.remove('animToAttach');
	animModelSquare.classList.remove('animDemo');
	stopButton.style.color = "grey";
	toggle = false;
}
 
function timer()
{
	delayMillis = (document.getElementById('num5').value) * 1000;
	
	myTimer = setTimeout(function()
			{
			   myButton.innerHTML = "&#9658";
			   myButton.title = "Start";
			   progressBox.classList.remove('animToAttach');
			   animModelSquare.classList.remove('animDemo');
			   stopButton.style.color = "grey";
			   toggle = false;
			}, delayMillis);
}
